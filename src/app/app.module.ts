import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

/* Inicializar Firebase */
import firebase from 'firebase';
var config = {
  apiKey: "AIzaSyDsev74-bMrzyx4yBsT6lgH5HO48btd-1I",
  authDomain: "ionicfirebase10.firebaseapp.com",
  databaseURL: "https://ionicfirebase10.firebaseio.com",
  projectId: "ionicfirebase10",
  storageBucket: "ionicfirebase10.appspot.com",
  messagingSenderId: "771435476026"
};
firebase.initializeApp(config);
firebase.firestore().settings({timestampsInSnapshots:true}); // nueva linea
/* Inicializar Firebase */

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular'; // Importamos AllertController desde ionic-angular

import firebase from 'firebase'; //Importamos firebase

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts: any[] = []; // variable para guardar los posts

  constructor(public navCtrl: NavController,
    public alerta: AlertController // instanciamos un nuevo objeto de AllertController en el cosnstructor
  ) {

  }

  ionViewDidLoad(){
    this.cargarPosts();
  }

  cargarPosts(){
    this.posts = [];
    firebase.firestore().collection("Posts").get() //Consulta a firebaes
    .then((datos) => { //Si salió bien
      datos.forEach(post => { //Recorrer el array
        this.posts.push(post); //Guardar el post en nuestra variable global
      })
    })
    .catch((error) => { //Si salió mal
      console.log(error.message);
    });
  }

  nuevoPost(){
    let alerta = this.alerta.create({ // creamos una nueva alerta, está recibe un objeto json con varios atributos
      title: "Nuevo post", //El titulo de la alerta
      message: "Agregar un nuevo post", //Mensaje para mostrar en la alerta
      inputs: [ // Los inputs nos permiten escribir datos en la alerta, hay de tipo text, number, password
        { // este array tambien recibe objetos json para crear cada input
          type: "text", // El tipo del input
          placeholder: "Autor", // Texto por defecto del input
          name: "autor" // Nombre con el que accederemos a los datos en el manejador de la alerta
        },
        {
          type: "text", // El tipo del input
          placeholder: "Mensaje", // Texto por defecto del input
          name: "mensaje" // Nombre con el que accederemos a los datos en el manejador de la alerta
        }
      ],
      buttons: [ //Otro array que permite mostrar botónes en la alerta, con los que haremos algo a lo que sean pulsados
        { //Tambien recibe ohjetos json
          text: "Cancelar", //Texto del botón
          handler: () => {} // Funcion que se ejecuta al pulsar el botón, este botón no hará nada
        },
        {
          text: "Guardar", 
          handler: (datos) => { //Recibimos por parametros los datos de los inputs
            // En el objeto datos tenemos lo que se ha escrito en el input, y es lo
            // que utilizaremos para enviar el nuevo post a firebase
            firebase.firestore().collection("Posts").add({ // Con la función add agregamos un nuevo post a la colección
              //Esta función tambien recibe un objeto json con los atributos que especifiquemos
              autor: datos.autor, // El mismo atributo que tenemos de autor, y accedemos al autor en la variable datos
              mensaje: datos.mensaje // Igual con el atributo mensaje
            })
            .then((documento) => { //Esta función tambien es una promesa por lo que podemos acceder a then y cacth
              this.cargarPosts();
              console.log("Documento nuevo creado");
            })
            .catch((error) => {
              console.log(error.message);
            });
          }
        }
      ]
    });
    alerta.present(); //Mostramos el alerta
  }

}
